
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");


const app = express();


mongoose.connect("mongodb+srv://admin123:admin123@project0.koqfldr.mongodb.net/capstone-2?retryWrites=true&w=majority",

	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}

	
);

// MongoDB Atlas connection status
let db = mongoose.connection;
db.on("error", () => {console.error.bind(console, "Connection error")});
db.once("open", () => {console.log("Now connected to MongoDB Atlas.")});

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// all user routes endpoints will start with "/users"
app.use("/users", userRoutes);

// all user routes endpoints will start with "/products"
app.use("/products", productRoutes);

// all order routes endpoints will start with "/users"
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});