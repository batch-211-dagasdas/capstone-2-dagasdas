// Order
// userId - string
// totalAmount - number,
// purchasedOn - date
// 		     default: new Date(),
// products - [

// 	{
// 		productId - string,
// 		quantity - number
// 	}

// ]

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "userId is blank"]
	},
	totalAmount : {
		type : Number,
		required : [true, "Total amount"]
	},
	purchasedOn : {
		type: Date,
				default: new Date()
	},
	products : [
		{
			productId:{
				type: String,
				required: [true,"productID is blank"]
			},
			quantity:{
				type: Number,
				required : [true, "Please provide quantity"]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);
