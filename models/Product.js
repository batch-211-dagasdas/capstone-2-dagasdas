
// Product
// name - string,
// description - string,
// price - number
// isActive - boolean
// 		   default: true,
// createdOn - date
// 			default: new Date()
// orders - [
// 	{
// 		orderId - string,
// 		quantity - number

// 	}
// ]


const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product Name is Required"]
	},
	description : {
		type : String,
		required : [true, "Product Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Please Input Product Price"]
	},
	createdOn : {
		type: Date,
				default: new Date()
	},
	isActive: {
		type: Boolean,
				default: true
	},
	orders : [
		{
			orderId:{
				type: String,
				required: [true,"order Id is required"]
			},
			quantity:{
				type: Number,
				required : [true, "Please provide quantity"]
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);
