// Import packages
const bcrypt = require("bcrypt");

// Import authentication js
const auth = require("../auth");

// Import model
const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");

// ----- Session 43 Deliverables ----- //

	// Add product
	// Admin Only
module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product({
				name: capitalizeAllFirst(data.product.name).trim(),
				description: data.product.description,
				price: data.product.price,
		});

		return Product.find({name: newProduct.name}).then((result) => {
			if(result.length > 0){
				return false;
			} else{
				return newProduct.save().then((product, error) => {
					if(error) {
						return false;
					} else {
						return true;
					};
				});
			}
		});
	} else{
		return denyAccess();
	}
}

	// Retrieve all active products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then((result) => {
		return result;
	});
}


// ----- Session 44 Deliverables ----- //
	
	// Retrieve specific product
module.exports.getProduct = (requestParams) => {
	return Product.findById(requestParams.productId).then((result) => {
		return result;
	});
}

	// Update product
	// Admin Only
module.exports.updateProduct = (requestParams, data) => {
	if(data.isAdmin){
		let updatedProduct = {
			name: capitalizeAllFirst(data.product.name).trim(),
			description: data.product.description,
			price: data.product.price
		};

		return Product.findByIdAndUpdate(requestParams.productId, updatedProduct).then((result, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	} else{
		return denyAccess();
	}	
}

	// Soft delete product (Update isActive status to "false")
	// Admin Only
module.exports.archiveProduct = (requestParams, data) => {
	if(data.isAdmin){
		return Product.findByIdAndUpdate(requestParams.productId, {isActive: false}).then((product, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});	
	} else {
		return denyAccess();
	}
}


// ----- Session 45 Deliverables ----- //

module.exports.createOrder = async (requestParams, requestBody) => {
	
	let isProductUpdated = await Product.findById(requestParams.productId).then((productResult) => {
		return User.findById(requestBody.userId.id).then((userResult) => {

			// --- Simplified Values START --- //
			let productResultId = productResult.id;
			let productResultName = productResult.name;
			let productOrders = productResult.orders;
			let userResultId = userResult.id;
			// --- Simplified Values END --- //

			let cartItem = {
				orderId: 1,
				quantity: requestBody.product.quantity,
			};

			productOrders.push(cartItem);

			return productResult.save().then((product, error) => {
				if(error){
					return false;
				} else{
					return true;
				}
			});
		});
	});



	let isOrderUpdated = await Product.findById(requestParams.productId).then((productResult) => {
		return User.findById(requestBody.userId.id).then((userResult) => {
				// --- Simplified Values START --- //
				let productResultId = productResult.id;
				let productResultName = productResult.name;
				let productOrders = productResult.orders;
				let userResultId = userResult.id;
				// --- Simplified Values END --- //

				if(requestBody.isAdmin !== true){
						let newOrder = new Order({
							userId: userResultId,
							products: [ 
								{
									productId: requestParams.productId,
									quantity: requestBody.product.quantity
								}
							],
							totalAmount: productResult.price * requestBody.product.quantity,
							purchasedOn: new Date(),
						});

						return newOrder.save().then((user, error) => {
							if(error){
								return false;
							} else{
								return true;
							}
						});
					} else{
						return denyAccess();
					}
		});
	});		
		
	return Product.findById(requestParams.productId).then((result) => {
		if(requestBody.isAdmin !== true){
			if(isProductUpdated && isOrderUpdated){
				return true;
			} else {
				return false;
			}
		} else{
			return denyAccess();
		}
	});
}
	
// ----- Extras and Stretch Goals ----- //

	// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result;
	});
}

	// Activate archived product (Update isActive status to "true")
	// Admin Only
module.exports.activateProduct = (requestParams, data) => {
	if(data.isAdmin){
		return Product.findByIdAndUpdate(requestParams.productId, {isActive: true}).then((product, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});	
	} else {
		return denyAccess();
	}
}

module.exports.getAllArchivedProducts = () => {
	return Product.find({isActive: false}).then((result) => {
		return result;
	});
}




// ----- Global ----- //

// Capitalization of the first letters of each word
function capitalizeAllFirst(string){
    
    // To remove the extra spaces in between each word
    let regexPattern = /\s+/g;
	let removedSpaces = string.replace(regexPattern, " ");

    let pieces = removedSpaces.split(" ");

    for (let i = 0; i < pieces.length; i++){
        let word = pieces[i].charAt(0).toUpperCase();
        pieces[i] = word + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}

// Failed Authorization
function denyAccess(){
	return Promise.resolve(false);
}