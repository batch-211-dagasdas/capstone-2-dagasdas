// Import packages
const bcrypt = require("bcrypt");

// Import authentication js
const auth = require("../auth");

// Import model
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return Order.find({}).then((result) => {
		return result;
		});
	} else {
		return denyAccess();
	}
}



// ----- GLOBAL FUNCTIONS ------ //

// Failed Authorization
function denyAccess(){
	return Promise.resolve("Not authorized to submit the request.");
}